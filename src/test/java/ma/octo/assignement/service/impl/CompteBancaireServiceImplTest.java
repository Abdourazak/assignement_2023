package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.repository.CompteBancaireRepository;
import ma.octo.assignement.service.CompteBancaireService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CompteBancaireServiceImplTest {


    private Utilisateur utilisateur;
    private CompteBancaire compteBancaire;
    @Mock
    CompteBancaireService compteBancaireService;

    @InjectMocks
    CompteBancaireServiceImpl compteBancaireServiceImpl ;

    @Mock
    CompteBancaireRepository compteBancaireRepository;

    @BeforeEach
    void initialisation() {
        MockitoAnnotations.initMocks(this);
        utilisateur = new Utilisateur("Username","lastname","firstname",new Date());
        compteBancaire = new CompteBancaire(2L,"Qhgh","RIB",new BigDecimal(8000),utilisateur);

    }



    @Test
    @DisplayName("obtenir compte bancaire en ayant fourni son RIB")
    void obtenirAbsolumentUnCompteBancaireApartirDeSonRib() throws CompteBancaireNonExistantException {
        when(compteBancaireServiceImpl.getCompteBancaireByRib("RIB")).thenReturn(compteBancaire);
        CompteBancaire compteBancaire2= compteBancaireServiceImpl.getCompteBancaireByRib("RIB");
        assertEquals(compteBancaire.getNrCompte(),compteBancaire2.getNrCompte());
    }

    @Test
    @DisplayName("Obtenir absolument un compte bancaire en ayant son numero")
    void etantDonneUnNumeroDeCompteDoitRetournerCeCompte() throws CompteBancaireNonExistantException {
       when(compteBancaireService.getCompteBancaireByNumCompte("num")).thenReturn(compteBancaire);
       CompteBancaire compteBancaire1 = compteBancaireService.getCompteBancaireByNumCompte("num");
       assertEquals(compteBancaire1,compteBancaire);
   }



}
package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.CompteBancaireService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TransferServiceImplTest {

    @Mock
    private TransferRepository transferRepository;

    @Mock
    private CompteBancaireService compteBancaireService;
    @Mock
    private AuditTransactionServiceImpl auditTransactionService;

    private Transfer transfer;
    @InjectMocks
    private TransferServiceImpl transferService;

    private CompteBancaire emetteur;
    private CompteBancaire beneficiaire;

    @BeforeEach
    void initialisation(){


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        CompteBancaire compte1 = new CompteBancaire();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200L));
        compte1.setUtilisateur(utilisateur1);
        emetteur=compte1;

        CompteBancaire compte2 = new CompteBancaire();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur1);
        beneficiaire=compte2;

        transfer = new Transfer();
        transfer.setMotifTransaction("fghjkl");
        transfer.setDateExecution(new Date());
        transfer.setCompteEmetteur(emetteur);
        transfer.setCompteBeneficiaire(beneficiaire);
    }




    @Test
    @DisplayName("Une exception est levee si la somme est inferieure a la somme minimale pour la transaction")
    void leverExceptionSiLaSommeestInferieureALaSommeMinimDeTransaction() throws CompteBancaireNonExistantException {
        TransferDto transferDto=new TransferDto("010000A000001000","010000B000001000","Motif1",new BigDecimal(7),new Date());
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire())).thenReturn(beneficiaire);

       TransactionException exception = assertThrows(TransactionException.class, () -> {
            transferService.executeTransfer(transferDto);
        });
        String expectedMessage = "Montant minimal de transfer non atteint";
        assertEquals(expectedMessage, exception.getMessage().toString());
    }

    @Test
    @DisplayName("Une exception est levee si la somme est superieure a la somme maximale de transaction")
    void leverExceptionSiLaSommeestSuperieureALaSommeMaximDeTransaction() throws CompteBancaireNonExistantException {

        TransferDto transferDto=new TransferDto("010000A000001000","010000A000001000","Motif1",new BigDecimal(10000000),new Date());
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire())).thenReturn(beneficiaire);

        TransactionException transactionException = assertThrows(TransactionException.class, () -> {
            transferService.executeTransfer(transferDto);
        });
        String messageAttendu = "Montant maximal de transfer dépassé";
        assertEquals(messageAttendu, transactionException.getMessage().toString());
    }



    @Test
    @DisplayName("Si le montant est vide léxception sera levee")
    void leverExceptionSiMontantEstVide() throws CompteBancaireNonExistantException {

        TransferDto transferDto=new TransferDto("010000K000001000","010000C000001000","Motif1",null,new Date());
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire())).thenReturn(beneficiaire);

        TransactionException transactionException=assertThrows(TransactionException.class,()->{
            transferService.executeTransfer(transferDto);
        });
        String messageAttendu="Montant vide";

        assertEquals(messageAttendu,transactionException.getMessage().toString());

    }

    @Test
    @DisplayName("Une exception est levee si le compte manque d argent pour un montant de transaction donne")
    void leverExceptionSiLeCompteManqueDargent() throws CompteBancaireNonExistantException {
        TransferDto transferDto=new TransferDto("010000K000001000","010000C000001000","Motif1",new BigDecimal(3000),new Date());
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire())).thenReturn(beneficiaire);

      SoldeDisponibleInsuffisantException soldeDisponibleInsuffisantException=assertThrows(SoldeDisponibleInsuffisantException.class,()->{
            transferService.executeTransfer(transferDto);
        });
        String messageAttendu="Solde insuffisant pour l'utilisateur";

        assertEquals(messageAttendu,soldeDisponibleInsuffisantException.getMessage().toString());
    }
    @Test
    @DisplayName("Si les infos sont correctes la transaction doit aboutir")
    void transactionDoitAboutirSiLesCredosSontCorrects() throws TransactionException, CompteBancaireNonExistantException, SoldeDisponibleInsuffisantException {
        TransferDto transferDto=new TransferDto("010000A000001000","010000A000001000","Motif1",BigDecimal.TEN,new Date());

        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur())).thenReturn(emetteur);
        when(compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire())).thenReturn(beneficiaire);

        transferService.executeTransfer(transferDto);

        verify(compteBancaireService, times(2)).getCompteBancaireByNumCompte(any());
        verify(compteBancaireService, times(2)).sauvegarderCompte(any());
        verify(transferRepository, times(1)).save(any());
        verify(auditTransactionService,times(1)).auditTransfer(any());
        assertNotEquals(transfer.getCompteEmetteur().getSolde(), emetteur.getSolde().subtract(transferDto.getMontant()));
        assertNotEquals(transfer.getCompteBeneficiaire().getSolde(), beneficiaire.getSolde().add(transferDto.getMontant()));
    }


}
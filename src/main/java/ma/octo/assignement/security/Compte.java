package ma.octo.assignement.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;

import javax.persistence.*;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/19/2022
 * Project name assignement_2023
 */
@Entity
@Data
@NoArgsConstructor
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private  String password;
    private  String login;

    @ManyToOne(optional = false)
    @JoinColumn(name = "utilisateur_id")
    private Utilisateur utilisateur;




}

package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@RestController
@RequestMapping("/api/utilisateur/")
public class UtilisateurController {
    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping("list-utilisateurs")
    public List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.getAllUtilisateurs();

    }

    @PostMapping("creer-utilisateur")
    public  void  creerUtilisateur(@RequestBody UtilisateurDto utilisateurDto){
          utilisateurService.creerUtilisateur(utilisateurDto);
    }

}

package ma.octo.assignement.web;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.dto.CompteBancaireDto;
import ma.octo.assignement.service.CompteBancaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@RestController()
@RequestMapping("/api/comptebancaire/")
public class CompteBancaireController {

    @Autowired
    CompteBancaireService compteBancaireService;

    @GetMapping("list-comptebancaires")
    List<CompteBancaire> loadAllCompte() {
        return  compteBancaireService.getAllCompteBancaire();
    }

    @PostMapping("creer-comptebancaire")
    public  void creerCompteBancaire(@RequestBody CompteBancaireDto compteBancaireDto){
        compteBancaireService.creerCompteBancaire(compteBancaireDto);
    }
}

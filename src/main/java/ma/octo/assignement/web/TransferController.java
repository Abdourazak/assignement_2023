package ma.octo.assignement.web;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/transferts/")
class TransferController {


    @Autowired
    private TransferService transferService;

    @GetMapping("list-transferts")

    List<Transfer> loadAllTransfers() {
      return   transferService.getAllTransfers();
    }


    @PostMapping("executer-transfer")
    @ResponseStatus(HttpStatus.CREATED)

    public void createTransfer(@RequestBody TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteBancaireNonExistantException, TransactionException {
          transferService.executeTransfer(transferDto);
    }

}

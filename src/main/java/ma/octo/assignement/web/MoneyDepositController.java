package ma.octo.assignement.web;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.MoneyDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/19/2022
 * Project name assignement_2023
 */
@RestController
@RequestMapping("/api/depots/")
public class MoneyDepositController {

    @Autowired
    MoneyDepositService moneyDepositService;

    /**
     *
     * @param moneyDepositDto
     * @throws TransactionException
     * @throws CompteBancaireNonExistantException
     */
    @PostMapping("deposer-argent")
    public  void deposerArgent(@RequestBody MoneyDepositDto moneyDepositDto) throws TransactionException, CompteBancaireNonExistantException {
        moneyDepositService.faireDepot(moneyDepositDto);

    }

    /**
     *
     * @return List<MoneyDeposit>
     */
    @GetMapping("/list-depots")
    public List<MoneyDeposit> getAllDepots(){
        return  moneyDepositService.getAllDepots();
    }
}

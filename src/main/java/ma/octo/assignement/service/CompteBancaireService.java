package ma.octo.assignement.service;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.dto.CompteBancaireDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;

import java.util.List;

public interface CompteBancaireService {

    public List<CompteBancaire> getAllCompteBancaire();
    public  void creerCompteBancaire(CompteBancaireDto compteBancaireDto);
    public CompteBancaire getCompteBancaireByNumCompte(String numeroCompte) throws CompteBancaireNonExistantException;
    public  void sauvegarderCompte(CompteBancaire compteBancaire);
    public  CompteBancaire getCompteBancaireByRib(String rib) throws CompteBancaireNonExistantException;

}

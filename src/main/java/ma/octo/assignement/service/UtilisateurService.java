package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
public interface UtilisateurService  {

    public List<Utilisateur> getAllUtilisateurs();
    public  void creerUtilisateur( UtilisateurDto utilisateurDto);

    Utilisateur getUtilisateur(Long id);
}

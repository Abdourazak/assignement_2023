package ma.octo.assignement.service;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface MoneyDepositService {
    public void faireDepot(MoneyDepositDto moneyDepositDto) throws CompteBancaireNonExistantException, TransactionException;
    public List<MoneyDeposit> getAllDepots();
}

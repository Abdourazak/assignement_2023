package ma.octo.assignement.service;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteAuthenticationException;
import ma.octo.assignement.security.Compte;
import ma.octo.assignement.service.utils.JwtUtil;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CompteService  extends UserDetailsService {
    public  void sauvegarderCompte(Compte compte);

}

package ma.octo.assignement.service.utils;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.CompteBancaireRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.security.Compte;
import ma.octo.assignement.service.AuditTransactionService;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/20/2022
 * Project name assignement_2023
 */
@Component
public class PopulateTestData {
    @Autowired
    private CompteBancaireRepository compteBancaireRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    CompteService compteService;
    @Autowired
    AuditTransactionService auditTransactionService;

    public  void populateDbWithData(){
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");

        utilisateurRepository.save(utilisateur2);

        CompteBancaire compteBancaire= new CompteBancaire();
        compteBancaire.setNrCompte("010000A000001000");
        compteBancaire.setRib("RIB1");
        compteBancaire.setSolde(BigDecimal.valueOf(200000L));
        compteBancaire.setUtilisateur(utilisateur1);

        compteBancaire=compteBancaireRepository.save(compteBancaire);

        CompteBancaire compteBancaire1 = new CompteBancaire();
        compteBancaire1.setNrCompte("010000B025001000");
        compteBancaire1.setRib("RIB2");
        compteBancaire1.setSolde(BigDecimal.valueOf(140000L));
        compteBancaire1.setUtilisateur(utilisateur2);

       compteBancaire1= compteBancaireRepository.save(compteBancaire1);

        Transfer t = new Transfer();
        t.setMontantTransaction(BigDecimal.TEN);
        compteBancaire.setSolde( compteBancaire.getSolde().subtract(t.getMontantTransaction()));
        compteBancaire1.setSolde(compteBancaire1.getSolde().add(t.getMontantTransaction()));
        t.setCompteBeneficiaire(compteBancaire1);
        t.setCompteEmetteur(compteBancaire);
        t.setDateExecution(new Date());
        t.setMotifTransaction("Assignment 2021");


        transferRepository.save(t);
        auditTransactionService.auditTransfer(t.getMotifTransaction());

        Compte compte=new Compte();
        compte.setLogin("login");
        compte.setPassword("1234");
        compte.setUtilisateur(utilisateur1);
        compteService.sauvegarderCompte(compte);
    }
}

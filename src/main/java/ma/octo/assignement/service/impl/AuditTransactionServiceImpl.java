package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.AuditTransaction;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransactionRepository;
import ma.octo.assignement.service.AuditTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@Service
@Transactional
public class AuditTransactionServiceImpl implements AuditTransactionService {

    Logger LOGGER = LoggerFactory.getLogger(AuditTransactionServiceImpl.class);

    @Autowired
    private AuditTransactionRepository auditTransactionRepository;


    /**
     *
     * @param message
     */
    @Override
    public void auditTransfer(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransaction auditTransaction = new AuditTransaction();
        auditTransaction.setEventType(EventType.TRANSFER);
        auditTransaction.setMessage(message);
        auditTransactionRepository.save(auditTransaction);
    }


    /**
     *
     * @param message
     */
    @Override
    public void auditDeposit(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);

        AuditTransaction auditTransaction = new AuditTransaction();
        auditTransaction.setEventType(EventType.DEPOSIT);
        auditTransaction.setMessage(message);
        auditTransactionRepository.save(auditTransaction);
    }
}

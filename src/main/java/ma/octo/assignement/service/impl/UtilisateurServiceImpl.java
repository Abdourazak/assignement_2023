package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@Service
public class UtilisateurServiceImpl implements UtilisateurService{
    private final UtilisateurRepository utilisateurRepository;

    /**
     * Nous faisons l'injecton de dependance
     * @param utilisateurRepository
     */
    @Autowired
    public  UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository){
        this.utilisateurRepository=utilisateurRepository;
    }


    /**
     * Cette methode recupere tous les usrs et les retourne
     * @return
     */
    @Override
    public List<Utilisateur> getAllUtilisateurs() {
        List<Utilisateur>utilisateurList=utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(utilisateurList)) {
            return null;
        } else {
            return utilisateurList;
        }
    }

    /**
     * Cette methode permet de creer les utilisateurs
     * @param utilisateurDto
     */
    @Override
    public void creerUtilisateur(UtilisateurDto utilisateurDto) {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername(utilisateurDto.getUsername());
        utilisateur1.setLastname(utilisateurDto.getLastname());
        utilisateur1.setFirstname(utilisateurDto.getFirstname());
        utilisateur1.setGender(utilisateurDto.getGender());
        utilisateurRepository.save(utilisateur1);



    }

    /**
     * Cette methode permet de recuperer un suel utilisateur
     * @param id
     * @return
     */
    @Override
    public Utilisateur getUtilisateur(Long id) {
        return utilisateurRepository.getById(id);
    }


}

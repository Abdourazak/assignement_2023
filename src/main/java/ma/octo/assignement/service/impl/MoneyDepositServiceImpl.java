package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.MoneyDepositRepository;
import ma.octo.assignement.service.CompteBancaireService;
import ma.octo.assignement.service.MoneyDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@Service
@Transactional
public class MoneyDepositServiceImpl  implements MoneyDepositService {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(MoneyDepositServiceImpl.class);
    @Autowired
    MoneyDepositRepository moneyDepositRepository;
    @Autowired
    CompteBancaireService compteBancaireService;

    @Autowired
    AuditTransactionServiceImpl auditTransactionService;

    /**
     * Cette methode permet de faire le depot
     * @param moneyDepositDto
     * @throws CompteBancaireNonExistantException
     * @throws TransactionException
     */
    @Override
    public void faireDepot(MoneyDepositDto moneyDepositDto) throws CompteBancaireNonExistantException, TransactionException{

        CompteBancaire cmptBeneficiaire = compteBancaireService.getCompteBancaireByRib(moneyDepositDto.getRib());


        if (cmptBeneficiaire == null) {
            System.out.println("Compte beneficaire Non existant");
            throw new CompteBancaireNonExistantException("Compte beneficiaire Non existant");
        }

        if (moneyDepositDto.getMontant()==null||moneyDepositDto.getMontant().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (moneyDepositDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (moneyDepositDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de depot non atteint");
            throw new TransactionException("Montant minimal de depot non atteint");
        } else if (moneyDepositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de depot dépassé");
            throw new TransactionException("Montant maximal de depot dépassé");
        }

        if (moneyDepositDto.getMotif()==null || moneyDepositDto.getMotif().length() <=0) {

            throw new TransactionException("Motif vide");
        }


        cmptBeneficiaire
                .setSolde(new BigDecimal(cmptBeneficiaire.getSolde().intValue() + moneyDepositDto.getMontant().intValue()));
        compteBancaireService.sauvegarderCompte(cmptBeneficiaire);

        MoneyDeposit moneyDeposit=new MoneyDeposit();
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setCompteBeneficiaire(cmptBeneficiaire);
        moneyDeposit.setNom_prenom_emetteur(moneyDepositDto.getNom_prenom_emetteur());
        moneyDeposit.setMontantTransaction(moneyDepositDto.getMontant());
        moneyDeposit.setMotifTransaction(moneyDepositDto.getMotif());
        moneyDepositRepository.save(moneyDeposit);

        auditTransactionService.auditDeposit("Depot fait par " + moneyDepositDto.getNom_prenom_emetteur()+ " vers le rib " +
               moneyDepositDto.getRib()+ " d'un montant de " + moneyDepositDto.getMontant()
                .toString());
    }

    /**
     * Cette partie retourne la liste des depots
     * @return
     */
    @Override
    public List<MoneyDeposit> getAllDepots() {
        LOGGER.info("Lister des depots");
        return moneyDepositRepository.findAll();



    }
}

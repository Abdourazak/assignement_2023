package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteBancaireDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.repository.CompteBancaireRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.CompteBancaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@Service
@Transactional
public class CompteBancaireServiceImpl  implements CompteBancaireService {
    @Autowired
    CompteBancaireRepository compteBancaireRepository;

    @Autowired
    UtilisateurRepository utilisateurRepository;


    /**
     *
     * @return List d utilisateurs
     */
    @Override
    public List<CompteBancaire> getAllCompteBancaire() {
        List<CompteBancaire> compteBancaireList= compteBancaireRepository.findAll();

        if (CollectionUtils.isEmpty(compteBancaireList)) {
            return null;
        } else {
            return compteBancaireList;
        }
    }

    /**
     * Ici nous creons le compte bancaire par cette methode
     * @param compteBancaireDto
     */
    @Override
    public void creerCompteBancaire(CompteBancaireDto compteBancaireDto) {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setNrCompte(compteBancaireDto.getNrCompte());
        System.out.println(compteBancaire.getNrCompte()+"Ceci est le compte");
        System.out.println(compteBancaireDto.getNrCompte()+"venant de postman");
        compteBancaire.setRib(compteBancaireDto.getRib());
        compteBancaire.setSolde(compteBancaireDto.getSolde());
        Utilisateur utilisateur=utilisateurRepository.getById(compteBancaireDto.getIdUtilisateur());
        compteBancaire.setUtilisateur(utilisateur);
        compteBancaireRepository.save(compteBancaire);

    }

    /**
     *
     * @param numeroCompte
     * @return CompteBancaire
     * @throws CompteBancaireNonExistantException
     */
    @Override
    public CompteBancaire getCompteBancaireByNumCompte(String numeroCompte) throws CompteBancaireNonExistantException {
        CompteBancaire compteBancaire= compteBancaireRepository.findByNrCompte(numeroCompte);
        if (compteBancaire== null) {

            throw new CompteBancaireNonExistantException("Compte  Non existant");
        }
        return  compteBancaire;
    }


    /**
     * Ici nous sauvegardons le compte de l'utilisateur
     * @param compteBancaire
     */

    @Override
    public void sauvegarderCompte(CompteBancaire compteBancaire) {
        compteBancaireRepository.save(compteBancaire);
    }

    /**
     *
     * @param rib
     * @return CompteBancaire
     * @throws CompteBancaireNonExistantException
     */
    @Override
    public CompteBancaire getCompteBancaireByRib(String rib) throws CompteBancaireNonExistantException {

        CompteBancaire compteBancaire= compteBancaireRepository.findByRib(rib);
        if (compteBancaire== null) {

        }

        return  compteBancaire;
    }
}

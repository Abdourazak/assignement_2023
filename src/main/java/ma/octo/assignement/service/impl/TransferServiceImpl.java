package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.CompteBancaireService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
@Service
@Transactional
public class TransferServiceImpl implements TransferService {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferServiceImpl.class);
    @Autowired
    TransferRepository transferRepository;
    @Autowired
    CompteBancaireService compteBancaireService;

    @Autowired
    AuditTransactionServiceImpl auditTransactionService;

    /**
     * Cette methode permet de faire le transfert d'un compte a un autre
     * @param transferDto
     * @throws CompteBancaireNonExistantException
     * @throws TransactionException
     * @throws SoldeDisponibleInsuffisantException
     */
    @Override
    public void executeTransfer(TransferDto transferDto) throws CompteBancaireNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        CompteBancaire cmptEmetteur = compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteEmetteur());
        CompteBancaire cmptBeneficiaire =compteBancaireService.getCompteBancaireByNumCompte(transferDto.getNrCompteBeneficiaire());

        if (transferDto.getMontant()==null||transferDto.getMontant().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif()==null || transferDto.getMotif().trim().length() <=0) {

            throw new TransactionException("Motif vide");
        }

        if (cmptEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw  new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }


        cmptEmetteur.setSolde(cmptEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteBancaireService.sauvegarderCompte(cmptEmetteur);

        cmptBeneficiaire
                .setSolde(new BigDecimal(cmptBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
        compteBancaireService.sauvegarderCompte(cmptBeneficiaire);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(new Date());
        transfer.setCompteBeneficiaire(cmptBeneficiaire);
        transfer.setCompteEmetteur(cmptEmetteur);
        transfer.setMontantTransaction(transferDto.getMontant());
        transfer.setMotifTransaction(transferDto.getMotif());

        transferRepository.save(transfer);

        auditTransactionService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }

    @Override
    public List<Transfer> getAllTransfers() {
        LOGGER.info("Lister des transfers");
        return transferRepository.findAll();

    }




}

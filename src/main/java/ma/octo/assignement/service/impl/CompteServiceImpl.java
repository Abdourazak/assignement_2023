package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteAuthenticationException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.security.Compte;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/20/2022
 * Project name assignement_2023
 */
@Service
public class CompteServiceImpl implements CompteService, UserDetailsService {
    @Autowired
    CompteRepository compteRepository;
    @Autowired
    UtilisateurService utilisateurService;
    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * Nous creons des comptes a travers cette methode
     * @param compte
     */
    @Override
    public void sauvegarderCompte(Compte compte) {
        compte.setPassword(passwordEncoder.encode(compte.getPassword()));
        compteRepository.save(compte);
    }


    /**
     *  Cette partie charge l'utilisateur par son login
     * @param username
     * @return UserDetails
     * @throws UsernameNotFoundException
     */

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Compte compte = compteRepository.findCompteByLogin(username);

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        Utilisateur utilisateur=compte.getUtilisateur();
       utilisateur.getRoles().forEach(r->{
            authorities.add(new SimpleGrantedAuthority(r.getRoleName()));
        });

        return new User(compte.getLogin(),compte.getPassword(),authorities);
    }
}

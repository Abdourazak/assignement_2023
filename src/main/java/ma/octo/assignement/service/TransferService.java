package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteBancaireNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface TransferService {

    public  void executeTransfer(TransferDto transferDto) throws CompteBancaireNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;
    public List<Transfer> getAllTransfers();

}

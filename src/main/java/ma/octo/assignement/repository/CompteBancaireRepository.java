package ma.octo.assignement.repository;

import ma.octo.assignement.domain.CompteBancaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteBancaireRepository extends JpaRepository<CompteBancaire, Long> {
  CompteBancaire findByNrCompte(String nrCompte);
  CompteBancaire findByRib(String rib);
}

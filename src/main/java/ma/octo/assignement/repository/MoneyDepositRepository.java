package ma.octo.assignement.repository;

import ma.octo.assignement.domain.MoneyDeposit;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/19/2022
 * Project name assignement_2023
 */
public interface MoneyDepositRepository  extends JpaRepository<MoneyDeposit,Long> {
}

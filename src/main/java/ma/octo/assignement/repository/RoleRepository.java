package ma.octo.assignement.repository;

import ma.octo.assignement.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/20/2022
 * Project name assignement_2023
 */
public interface RoleRepository  extends JpaRepository<Role,Long> {
}

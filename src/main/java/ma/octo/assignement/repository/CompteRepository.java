package ma.octo.assignement.repository;

import ma.octo.assignement.security.Compte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte,Long> {
     Compte findCompteByLogin(String login);
}

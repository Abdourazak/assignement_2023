package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MONEY_DEPOSIT")
@PrimaryKeyJoinColumn(name = "idDeposit")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyDeposit extends  Transaction {

  @Column
  private String nom_prenom_emetteur;

  public String getNom_prenom_emetteur() {
    return nom_prenom_emetteur;
  }

  public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
    this.nom_prenom_emetteur = nom_prenom_emetteur;
  }
}

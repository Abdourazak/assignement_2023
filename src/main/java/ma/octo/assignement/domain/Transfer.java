package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "TRANSFER")
@PrimaryKeyJoinColumn(name = "idTransfer")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transfer extends  Transaction {
  @ManyToOne
  private CompteBancaire compteEmetteur;
  public CompteBancaire getCompteEmetteur() {
    return compteEmetteur;
  }
  public void setCompteEmetteur(CompteBancaire compteEmetteur) {
    this.compteEmetteur = compteEmetteur;
  }


}

package ma.octo.assignement.exceptions;

public class CompteBancaireNonExistantException extends Exception {

  private static final long serialVersionUID = 1L;

  public CompteBancaireNonExistantException() {
  }

  public CompteBancaireNonExistantException(String message) {
    super(message);
  }
}

package ma.octo.assignement.exceptions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/20/2022
 * Project name assignement_2023
 */

public class CompteAuthenticationException extends  Exception{
     public CompteAuthenticationException(String message){super(message);}
}

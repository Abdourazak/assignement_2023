package ma.octo.assignement.dto;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
public class UtilisateurDto {

    private String username;


    private String gender;

    private String lastname;

    private String firstname;

    private Date birthdate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}

package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/19/2022
 * Project name assignement_2023
 */
public class MoneyDepositDto {
    private String nom_prenom_emetteur;


    private String motif;
    private BigDecimal montant;
    private Date date;
    private  String rib;

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getNom_prenom_emetteur() {
        return nom_prenom_emetteur;
    }

    public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
        this.nom_prenom_emetteur = nom_prenom_emetteur;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

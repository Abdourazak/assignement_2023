package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/20/2022
 * Project name assignement_2023
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompteDto {
    private  String login;
    private  String password;
}

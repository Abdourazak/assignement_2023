package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Utilisateur;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by  TETEREOU Aboudourazakou   on  11/18/2022
 * Project name assignement_2023
 */
public class CompteBancaireDto {

    private String nrCompte;

    private String rib;

    private BigDecimal solde;

    private Long idUtilisateur;

    public String getNrCompte() {
        return nrCompte;
    }

    public void setNrCompte(String nrCompte) {
        this.nrCompte = nrCompte;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public Long getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Long idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }
}

package ma.octo.assignement;

import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.CompteBancaire;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.*;
import ma.octo.assignement.repository.CompteBancaireRepository;
import ma.octo.assignement.security.Compte;
import ma.octo.assignement.service.AuditTransactionService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.utils.PopulateTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
@Transactional
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	PopulateTestData populateTestData;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
            populateTestData.populateDbWithData();
	}
}
